# buddyclean

Turner Hall 2020 - Licensed NPLv4+

## Description

Work in progress:
Demonstrative simulation of CLEAN/SWEEP protocol written in scala with scalafx

[Video](assets/buddyclean-finished.webm)

Replicating and expanding on the SWEEP/CLEAN protocol

## Project Dependencies:

- `scala 2.13.0`
- `sbt`
- Java 11 (`jdk`, `jre`, and `openjfx`)

## Run Instructions:

- open sbt with: `sbt`
- `compile`
- `run`
- (optional) build an executable jar with `assembly`
