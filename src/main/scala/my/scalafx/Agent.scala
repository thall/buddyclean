package my.scalafx

import my.scalafx.EntityType._
import my.scalafx.Face._
import my.scalafx.Action._
import scala.collection.mutable.ListBuffer

class Agent(
    private var _x: Int,
    private var _y: Int,
    private var _face: Face.Value
) extends GridEntity {

  // movement log not used in agent behavior
  private var moveLog: ListBuffer[((Int, Int), Face.Value)] = ListBuffer()
  private var _lastOnFringe = true // we assume we start on the fringe
  private var lastCoord: (Int, Int) = (-1, -1)
  private var occupied = false // no other agents at x,y
  private var bridgeMode = false
  private var bridgeModeCooldown = 0
  private var lastOnMark = false
  private var bridgeIgnoreCooldown = 0
  // how many ticks after the cooldown of 9 do we give up?
  val searchAfterBridgeBreak = 7
  // number of ticks to ignore bridges after one breaks under us
  val bridgeIgnoreCooldownDefault = 9
  // how many ticks do we wait to clean after bridging?
  val bridgeModeCooldownDefault = 2
  // constants representing special grid environment cases
  // with face right
  val emptySquare = List(List(false,false,false),List(false,false),List(false,false,false))
  val tetrominoException = rotateCases(List(
    List(false, true, false),
    List(true, true),
    List(false, false, false)),Right)
  val offCenterTetException = rotateCases(List(
    List(true, true, true),
    List(false, false),
    List(false, false, false)
  ),Left)
  val lNibException = rotateCases(List(
    List(true, true, false),
    List(false, false),
    List(false, false, false)
  ),Left)
  val sCurveException = rotateCases(List(
    List(true, false, false),
    List(true, false),
    List(false, true, true)
  ),Right)

  def setLastCoord(coord: (Int, Int)): Unit = { lastCoord = coord }
  def setLastOnFringe(x: Boolean): Unit = { _lastOnFringe = x }

  def lastOnFringe(): Boolean = _lastOnFringe

  // moves agent and returns true or false whether or not to clean current tile
  // dirt is the dirty value of the square the agent is on
  def process(
      front: List[Boolean],
      squares: (List[List[Boolean]], List[List[Boolean]]),
      dirtAndMark: (Boolean,Boolean),
      lastDirtAndMark: (Boolean,Boolean),
    ahead: List[(Boolean,(Int,Int))]
  ): Boolean = {
    var (square, marked) = squares
    val (dirt, mark) = dirtAndMark
    val (lastDirt, markBehind) = lastDirtAndMark
    // set markAdjacent if adjacent to a mark
    val markAdjacent = isFourAdjacent(marked)
    // detect if our bridge has "broken"
    if(bridgeMode && lastOnMark && !marked.contains(true)) bridgeMode = false
    // check whether to ignore marked tiles:
    if (bridgeIgnoreCooldown != 0) {
      marked = emptySquare
      if (square == emptySquare) {

      }
    }
    // enter bridge mode if necessary
    val doClean = if (markAdjacent && bridgeModeCooldown <= 0 && !bridgeMode && (bridgeIgnoreCooldown == 0)) {
        bridgeMode = true
        // wait 2 ticks after bridgeMode to clean
        bridgeModeCooldown = bridgeModeCooldownDefault
        // move onto first marked tile, facing its direction
        val nearDir = nearestNeighbor(marked)
        move(nearDir)
        false
    } else if (bridgeMode) { // this state assumes we are facing across bridge
      // move and reevaluate if ahead tile is still a bridge/marked tile
      // if ahead tile is a bridge, continue bridgeMode
      val aheadMarked = nextGridTileTrue(marked)
      if (!aheadMarked) {
        bridgeMode = false
        // ignore bridges for a tick
        bridgeIgnoreCooldown = bridgeIgnoreCooldownDefault
      }
      move(face)
      false
    } else { // normal fringe-following behavior
      val cMoveStatus: ((Int, Int), Face.Value) = ((_x, _y), face)
      moveLog = moveLog += cMoveStatus
      val coolingDown = if (bridgeIgnoreCooldown >= searchAfterBridgeBreak) true else false
      val (actionList, clean) = getActionList(front, square, dirt, lastDirt, coolingDown)
      // DEBUG: println("Entity: "+(_x,_y)+" ActionList: "+actionList)
      processActions(actionList, true)
      // do not clean during cooldown after a bridge crossing
      if (bridgeModeCooldown >= 0) {
        bridgeModeCooldown -= 1
        false
      }
      else clean
    }
    // wrap up and save lastValues
    // if we ignored a bridge, stop doing that next tick
    if (markAdjacent && bridgeModeCooldown <= 0 && !bridgeMode && (bridgeIgnoreCooldown!=0)) bridgeIgnoreCooldown -= 1
    lastOnMark = mark
    doClean
  }
  def toPlaceMarking():Boolean = false
  // return a list of moves taken if we've circled the area
  // otherwise return empty list - this is used to scout the fringe
  def fetchMoveLog(): List[((Int, Int), Face.Value)] = {
    val moves = moveLog.toList
    if (moves.isEmpty) {
      List()
    } else {
      // have we returned to the start coordinate?
      val returned =
        if (moves.map(x => x._1).tail.contains(moveLog.head._1)) true else false
      if (returned) moves.distinctBy(x => x._1) else List()
    }
  }

  // this function does not work for the very first move, assuming proper initialization
  def priority(): Int = {
    val (lastX, lastY) = lastCoord
    (2 * (lastX - x) + (lastY - y))
  }

  def entityType: EntityType.Value = AgentEntity;
  def x = _x
  def y = _y
  def face = _face
  def location = (_x, _y)
  def occupy(oX: Int, oY: Int): Unit = {
    if (x == oX && y == oY) occupied = true
  }
  def last = {
    if (lastCoord != (-1, -1)) lastCoord
    else { // we're going to assume starting face is accurate
      face match {
        case Up    => (_x, _y + 1)
        case Down  => (_x, _y - 1)
        case Left  => (_x - 1, _y)
        case Right => (_x + 1, _y)
      }
    }
  }
  // move in specified direction and reset face to that direction
  def move(dir: Face.Value): Unit = {
    lastCoord = (_x, _y)
    dir match {
      case Up    => _y -= 1
      case Down  => _y += 1
      case Left  => _x -= 1
      case Right => _x += 1
    }
    _face = dir
  }

  // return the next coordinate in front of the agent
  def advanceCoord(): (Int, Int) = {
    var (dx, dy) = (_x, _y)
    face match {
      case Up    => dy -= 1
      case Down  => dy += 1
      case Left  => dx -= 1
      case Right => dx += 1
    }
    (dx, dy)
  }

  // return the next coordinate if the agent were to act now
  def nextCoord(
      front: List[Boolean],
      square: List[List[Boolean]],
      dirt: Boolean,
      lastDirt: Boolean
  ): (Int, Int) = {
    val (actionList, _) = getActionList(front, square, dirt, lastDirt, false)
    processActions(actionList, false)
  }


  private def isOnFringe(eightSquares: List[List[Boolean]]): Boolean =
    eightSquares.flatten.exists(x => !x)

  private def isCritical(squares: List[List[Boolean]]): Boolean = {
    // 2,4,!1 - 2,5,!3 - 4,7,!6 - 5,7,!8 - etc
    val ret = squares match {
      case List(List(false, true, _), List(true, _), _) => true
      case List(List(_, true, false), List(_, true), _) => true
      case List(_, List(true, _), List(false, true, _)) => true
      case List(_, List(_, true), List(_, true, false)) => true
      case List(List(_, x, _), List(true, true), List(_, y, _)) =>
        if (!x && !y) true else false
      case List(List(_, true, _), List(x, y), List(_, true, _)) =>
        if (!x && !y) true else false
      case _ => false
    }
    ret
  }

  /**    take the list of actions and determine the resulting coordinate of the agent
    *    were the agent to take those actions. If execute is true, the agent is actually
    *    told to take these actions, otherwise this is used purely for projecting the
    *    destination of the agent for the next tick.
    */
  private def processActions(
      actions: ListBuffer[Action.Value],
      execute: Boolean
  ): (Int, Int) = {
    val (cX, cY) = (x, y) // store current coordinates
    val cFace = face // store current face
    // execute the list of actions
    actions.foreach(a =>
      a match {
        case Move      => move(face)
        case LeftTurn  => turn(Left)
        case RightTurn => turn(Right)
      }
    )
    val ret = (x, y)
    // undo actions if we are not supposed to actually execute
    if (!execute) {
      _x = cX
      _y = cY
      _face = cFace
    }
    ret
  }

  // TODO: if coolingdown, turn right and start return to fringe

  // returns a list of actions for the agent to take and a boolean
  // to determine whether or not the agent should clean the current tile
  private def getActionList(
    front: List[Boolean],
    square: List[List[Boolean]],
    dirt: Boolean,
    lastDirt: Boolean,
    coolingDown: Boolean
  ): (ListBuffer[Action.Value], Boolean) = {
    var actionList = new ListBuffer[Action.Value]()
    val critical = isCritical(square) && dirt
    // don't clean if is-critical
    var clean = if (critical) false else true
    // follow fringe if on the fringe
    val nowOnFringe = isOnFringe(square)
    if (!nowOnFringe && lastOnFringe) {
      // spread has occured, begin search for the fringe
      actionList += LeftTurn
      actionList += Move
      clean = false
    } else if (!nowOnFringe && !lastOnFringe) {
      // We are still lost, continue search
      actionList += Move
      clean = false
    } else if (nowOnFringe) {
      // Follow the Fringe behavior
      val (x :: y :: z :: l :: r) = front
      // x,y,z are from left to right the three tiles in front of the agent
      // l and r are the tiles directly to the left and right of the agent
      // we have reached the end of a critical tile line, turn around
      if (!z && lastDirt && !y && !front.last) {
        actionList += RightTurn
        actionList += RightTurn
        actionList += Move
        actionList += LeftTurn
      } else {
        // decide between moving before or after turning depending on
        // whether a fringe tile is available to turn into (left)
        if (x && y && l) {
          actionList += LeftTurn
          actionList += Move
        } else if ((!x && y && z && l)) {
          actionList += Move
        } else if (x && y) {
          actionList += Move
          actionList += LeftTurn
        } else if (!y) {
          actionList += RightTurn
          actionList += Move
          // sub-case for bug? when to reorient?
        } else actionList += Move
      }
    }
    // done with usual fringe-follow
    // exceptions
    val ourCase = (square, _face)
    if (tetrominoException.exists(_ == ourCase)
      || offCenterTetException.exists(_ == ourCase)
      || lNibException.exists(_ == ourCase)
      || sCurveException.exists(_ == ourCase)) {
      actionList += LeftTurn
    }
    // wrapping up the results
    _lastOnFringe = nowOnFringe
    (actionList, clean)
  }

  private def turn(dir: Face.Value): Unit = {
    val newdir = relToAbsolute(dir)
    _face = newdir
  }

  // return a list of four cases for each rotation of 90 degrees
  private def rotateCases(square:List[List[Boolean]], cface: Face.Value):List[(List[List[Boolean]],Face.Value)] = {
    var currentCase = (square,cface)
    var caseList:List[(List[List[Boolean]],Face.Value)] = List()
    // now we build our cases (haha)
    for (i <- 0 until 4) {
      caseList = currentCase::caseList
      currentCase = (rotateSquare90(currentCase._1),rotate90(currentCase._2))
    }
    caseList
  }

  private def rotateCase(square:List[List[Boolean]], cface:Face.Value):(List[List[Boolean]],Face.Value) = {
    (rotateSquare90(square),rotate90(_face))
  }

  private def rotateSquare90(square:List[List[Boolean]]): List[List[Boolean]] = {
    // editing mid row to have three values so square is well, square
    val squareSeq = square
    val squareMid = squareSeq(1)
    val squareMidNew = List(squareMid(1),true,squareMid(0)).reverse
    val properSquare = squareSeq.updated(1,squareMidNew)
    // transpose
    val transSquare = properSquare.transpose
    // reverse rows
    val rotatedTransSquare = transSquare.map(_.reverse)
    // remove the dummy true value to return to not storing (1)(1)
    val rotatedMid = rotatedTransSquare(1)
    val fixedRotatedMid = List(rotatedMid(2),rotatedMid(0)).reverse
    List(rotatedTransSquare(0),fixedRotatedMid,rotatedTransSquare(2))
  }
  private def relToAbsolute(reldir: Face.Value): Face.Value = {
    val newFace = (_face, reldir) match {
      // understand the second column as "Forward, Backward, Left, Right"
      case (Up, Up)       => Up
      case (Up, Down)     => Down
      case (Up, Left)     => Left
      case (Up, Right)    => Right
      case (Down, Up)     => Down
      case (Down, Down)   => Up
      case (Down, Left)   => Right
      case (Down, Right)  => Left
      case (Left, Up)     => Left
      case (Left, Down)   => Right
      case (Left, Left)   => Down
      case (Left, Right)  => Up
      case (Right, Up)    => Right
      case (Right, Down)  => Left
      case (Right, Left)  => Up
      case (Right, Right) => Down
      case _ => {
        Console.err.println(
          Console.RED + "relToAbsolute Failed" + Console.RESET
        )
        sys.exit(1)
      }
    }
    newFace
  }
  def toXML(): xml.Elem = {
    <gridEntity>
      <etype>agent</etype>
      <xCoord>{x}</xCoord>
      <yCoord>{y}</yCoord>
      <face>{face}</face>
      <lastCoord>{last}</lastCoord>
      <lastOnFringe>{lastOnFringe}</lastOnFringe>
    </gridEntity>
  }
}

object Agent {}
