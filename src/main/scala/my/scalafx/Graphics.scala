package my.scalafx

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.event.ActionEvent
import scalafx.scene.control._
import scalafx.scene.layout.BorderPane
import scalafx.scene.layout.FlowPane
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint._
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.animation._
import scalafx.Includes._
import scalafx.stage.FileChooser
import scalafx.stage.FileChooser.ExtensionFilter

import scala.xml._

import my.scalafx.Grid._
import my.scalafx.GridRenderer._
import my.scalafx.EntityType._
import my.scalafx.Face._
import my.scalafx.Simulation._

class Graphics(data: (String, String)) extends JFXApp {
  // default is 40x40 grid with squares of size 22 (20px empty inner space each)
  var grid = new Grid(40, 22)
  // change this value to modify sim speed
  var refreshInterval = 0.5
  var playState = false

  stage = new JFXApp.PrimaryStage {
    title.value = "buddy clean"
    scene = new Scene(grid.getCanvasSz, grid.getCanvasSz + 30) {
      var canvas = new Canvas(grid.getCanvasSz, grid.getCanvasSz)
      var gc = canvas.graphicsContext2D

      // create the gridRenderer
      var gr = new GridRenderer(gc)
      var sim = new Simulation(Some(gr), grid)
      // do we have something to load in instead?
      if (data._1 == "load") {
        val simFile = data._2
        println("Loading: " + simFile)
        val xmlData = XML.loadFile(simFile)
        // load simulation
        val getSz = 40
        val getSquareSz = 22
        val canvasSz = 2 + (getSz * getSquareSz)
        // TODO: the get above values out of xmlData and resetup simulation
        canvas = new Canvas(canvasSz, canvasSz)
        gc = canvas.graphicsContext2D
        gr = new GridRenderer(gc)
        sim = Simulation.fromXML(Some(gr), xmlData)
      } else sim.fuzzySpreadSetup()

      // GUI stuff
      val tickLabel = new Label("Tick: 0    ")
      val lastMsgLabel = new Label("    Message: None")
      root = new BorderPane {
        val pane = new FlowPane
        center = canvas
        val playToggleButton = new Button("Play/Pause")
        val spacerLabel = new Label("    ")
        val speedLabel = new Label("Speed: " + refreshInterval + "    ")
        val speedButton = new Button("+Speed")
        val slowButton = new Button("-Speed")
        val playLabel = new Label("State: Running" + "    ")
        val saveButton = new Button("Save")
        speedButton.onAction = handle => {
          refreshInterval -= 0.2
          speedLabel.text = "Speed: " + refreshInterval + "    "
        }
        slowButton.onAction = handle => {
          refreshInterval += 0.2
          speedLabel.text = "Speed: " + refreshInterval + "    "
        }
        playToggleButton.onAction = handle => {
          playState = if (playState) false else true
          val playStateText: String = if (playState) "Running" else "Paused"
          playLabel.text = "State: " + playStateText + "    "
        }
        saveButton.onAction = handle => {
          playState = false
          playLabel.text = "State: Paused"
          XML.save("tick-" + sim.getTick() + ".xml", sim.toXML(), "UTF-8")
          lastMsgLabel.text = "    Message: Saved Successfully  "
        }

        pane.children = List(
          playToggleButton,
          speedButton,
          slowButton,
          spacerLabel,
          playLabel,
          tickLabel,
          speedLabel,
          saveButton,
          lastMsgLabel
        )
        top = pane
      }

      /**        Setting up the simulation:
        *        Unless you want to skip to a particular test case, run sim.setup()
        *        Once simulation ends (reaches specified number of ticks) next test
        *        case simulation will run automatically, repeating until all tests
        *        have been run.
        */
      // the grid gets updated below
      var lastTime = 0L
      var refreshdelay = 0.0
      val timer = AnimationTimer(t => {
        if (lastTime > 0 && playState) { // do stuff
          val delta = (t - lastTime) / 1e9
          refreshdelay += delta
          if (refreshdelay >= refreshInterval) {
            sim.update()
            sim.logTickData()
            tickLabel.text = "Tick: " + sim.getTick() + "    "
            lastMsgLabel.text = "    Message: " + sim.getMsg() + "  "
            refreshdelay = 0.0
          }
        }
        lastTime = t
      })
      timer.start
    }
  }
}
