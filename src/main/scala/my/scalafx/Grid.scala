package my.scalafx

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint._
import scalafx.scene.text.Text
import scalafx.scene.canvas._
import scalafx.application.Platform

import my.scalafx.Simulation._
import my.scalafx.GridRenderer._
import my.scalafx.Face._

class Grid(gridSz: Int, gridSquareSz: Int) {

  // change to (int,int) -> Entity later
  private var gridDirt = collection.mutable.Map[(Int, Int), Boolean]()
  private var gridMarkings = collection.mutable.Map[(Int, Int), Boolean]()

  // read the coordinate and return true if dirt, false otherwise
  def at(coord: (Int, Int)): Boolean = {
    if (gridDirt.contains(coord)) {
      gridDirt(coord._1, coord._2)
    } else false;
  }

  // read the coordinate and return true if marking, false otherwise
  def markingAt(coord: (Int, Int)): Boolean = {
    if (gridMarkings.contains(coord)) {
      gridMarkings(coord._1, coord._2)
    } else false;
  }

  def forwardScan(e:GridEntity,lookAhead:Int):List[(Boolean,(Int,Int))] = {
    // a forward scan returning a list of dirt/non-dirt blocks ahead
    var cx = e.x
    var cy = e.y
    var peek:List[(Boolean,(Int,Int))] = List()
    for (x <- 0 until lookAhead) {
      e.face match {
        case Up => cy-=1
        case Down => cy+=1
        case Left => cx-=1
        case Right => cx+=1
      }
      // println(cx.toString+" "+cy.toString)
      peek = (at((cx,cy)),(cx,cy))::peek
    }
    peek.reverse
  }

  // return all the coordinates of the fringe in a list
  // check if grid contains dirt before calling, beyond tick 0
  def fringeList(): List[((Int, Int), Face.Value)] = {
    // TODO: actually write a copy function for Grid
    val gridCopy = new Grid(40, 22)
    for (((x, y), b) <- gridDirt) if (b) gridCopy.addDirt(x, y)
    // testing
    val fringeSim = new Simulation(None, gridCopy)
    fringeSim.scoutFringe() // setup fringe-finding simulation
    var isFringeFound = false
    var fringeList: List[((Int, Int), Face.Value)] = List()
    // this just busywaits until the fringe has been found
    while (!isFringeFound) {
      // keep asking simulation if the fringe is found
      val maybeFringe = fringeSim.fetchFringeList
      if (maybeFringe != None) {
        isFringeFound = true
        fringeList = maybeFringe.get
      }
      fringeSim.update
    }
    fringeList
  }

  def fetchAFringeCoord(): (Int, Int) = {
    val fringeCoords = gridDirt.filter(c => isOnFringe(c._1))
    fringeCoords.head._1
  }

  // tests if a coordinate is on the fringe
  def isOnFringe(coord: (Int, Int)) = {
    val (x, y) = coord
    (!at(x - 1, y) || !at(x - 1, y - 1) || !at(x, y - 1)
    || !at(x + 1, y - 1) || !at(x + 1, y) || !at(x + 1, y + 1)
    || !at(x - 1, y + 1) || !at(x, y + 1))
  }

  // Trigger a dirt spreading event. All fringe-adjacent tiles will become dirty.
  def spread(): Unit = {
    val dirtySpots = gridDirt.filter(d => d._2 == true).keys
    dirtySpots.foreach(coord => spreadFrom(coord._1, coord._2))
  }

  def clean(): Boolean = !gridDirt.exists(x => x._2 == true)

  // number of grid squares on one axis
  def getSz(): Int = gridSz

  // number of pixels to use to draw a square on one axis
  // (includes the two used for drawing the surrounding grid lines)
  def getSquareSz(): Int = gridSquareSz

  // get the size of a canvas needed to display this grid
  def getCanvasSz(): Int = 2 + (getSz * getSquareSz)

  // dirt that overlaps a marking needs to erase the marking
  def addDirt(x: Int, y: Int): Unit = {
    gridDirt((x, y)) = true
    if (markingAt((x,y))) removeMarking(x,y) 
  }

  def removeDirt(x: Int, y: Int): Unit = gridDirt((x, y)) = false

  def clearDirt(): Unit = {
    gridDirt.clear()
    gridMarkings.clear()
  }

  def drawDirt(gr: GridRenderer): Unit = {
    gr.refreshGrid(getCanvasSz)
    for (((x, y), b) <- gridDirt) if (b) {
      if (isOnFringe(x, y)) gr.drawFringeAt(x, y) else gr.drawDirtAt(x, y)
    }
  }

  // markings are the pheremones created by bridging agents
  def addMarking(x: Int, y: Int): Unit = if (!markingAt((x,y))) gridMarkings((x, y)) = true

  private def removeMarking(x: Int, y: Int): Unit = gridMarkings((x, y)) = false

  def clearMarkings(): Unit = gridMarkings.clear()

  def drawMarkings(gr: GridRenderer): Unit = {
    for (((x,y),b) <- gridMarkings) if (b) gr.drawMarkingAt(x,y)
  }

  // draw a rectangle of dirt with p1 being the top-left point
  // and p2 being the bottom-right point
  def dirtRect(p1: (Int, Int), p2: (Int, Int)): Unit = {
    val (a, c) = p1
    val (b, d) = p2
    for (x <- a to b; y <- c to d) addDirt(x, y)
  }

  private def spreadFrom(x: Int, y: Int): Unit = {
    val above: (Int, Int) = (x, y - 1)
    val below: (Int, Int) = (x, y + 1)
    val left: (Int, Int) = (x - 1, y)
    val right: (Int, Int) = (x + 1, y)
    val toDirty: List[(Int, Int)] = List(above, below, left, right)
    // add the new dirt spread to the surrounding four neighbors
    toDirty.foreach(d => addDirt(d._1, d._2))
  }

  def toXML(): xml.Elem = {
    <grid>
    <gridSize>{gridSz}</gridSize>
    <gridSquareSz>{gridSquareSz}</gridSquareSz>
    <dirt>
    { // save all the dirty tile locations
      gridDirt
        .filter(_._2 == true)
        .map(x => {
          <tile>
        <xCoord>{x._1._1}</xCoord>
        <yCoord>{x._1._2}</yCoord>
        </tile>
        })
    }
    </dirt>
    <marks>
    { // save the marked tile locations
      gridMarkings
        .filter(_._2 == true)
        .map(x => {
          <tile>
        <xCoord>{x._1._1}</xCoord>
        <yCoord>{x._1._2}</yCoord>
        </tile>
        })
    }
    </marks>
    </grid>
  }
}

object Grid {

  def fromXML(gridData: xml.NodeSeq): Grid = {
    val gridSize = (gridData \ "gridSize").text.toInt
    val gridSquareSz = (gridData \ "gridSquareSz").text.toInt
    var ret = new Grid(gridSize, gridSquareSz)
    val dirtTiles = (gridData \ "dirt" \ "tile").map(t => {
      val xCoord = (t \ "xCoord").text.toInt
      val yCoord = (t \ "yCoord").text.toInt
      (xCoord, yCoord)
    })
    if ((gridData \ "marks" \ "tile").length >= 1) {
      val markedTiles = (gridData \ "marks" \ "tile").map(t => {
        val xCoord = (t \ "xCoord").text.toInt
        val yCoord = (t \ "yCoord").text.toInt
        (xCoord, yCoord)
      })
      markedTiles.foreach(t => ret.addMarking(t._1, t._2))
    }
    dirtTiles.foreach(t => ret.addDirt(t._1, t._2))
    ret
  }

}
