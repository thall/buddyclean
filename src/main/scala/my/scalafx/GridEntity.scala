package my.scalafx
import scala.xml._

trait GridEntity {
  import my.scalafx.Face._
  def entityType: EntityType.Value
  // coordinates on grid
  def x: Int
  def y: Int
  // direction being faced (change to a Direction Enumerator type later (rotate, etc))
  def face: Face.Value
  // move in a direction
  def move(dir: Face.Value): Unit
  // give last known location before moving
  def last: (Int, Int)
  def location: (Int, Int)
  // temporarily a boolean that just checks the grid square "ahead"
  def process(
    front: List[Boolean],
    square: (List[List[Boolean]], List[List[Boolean]]),
    dirtAndMark: (Boolean,Boolean),
    lastDirtAndMark: (Boolean,Boolean),
    ahead: List[(Boolean,(Int,Int))]
  ): Boolean
  // give the next coordinate that the agent would go to following its typical actions
  def nextCoord(
      front: List[Boolean],
      square: List[List[Boolean]],
      dirt: Boolean,
      lastDirt: Boolean
  ): (Int, Int)
  // look ahead one coordinate by the current face direction
  def advanceCoord(): (Int, Int)
  // set this entity's current tile to occupied if on the same coordinates as (oX,oY)
  def occupy(oX: Int, oY: Int): Unit
  // this function does not work for the very first move, assuming proper initialization
  def priority(): Int
  // give a move log if entity has navigated the full fringe
  def fetchMoveLog(): List[((Int, Int), Face.Value)]
  // assign an agent their last visited coordinate value
  def setLastCoord(coord: (Int, Int))
  // convert agent to an XML representation of itself
  def toXML(): xml.Elem
  // return true if entity was last on the fringe
  def lastOnFringe(): Boolean
  // special query for bridger agents, do we place a pheromone marker?
  def toPlaceMarking():Boolean
  // takes in a square of tiles as defined by gridInfo in Simulation
  // and checks if any of the 4-neighbors is true
  def isFourAdjacent(squares: List[List[Boolean]]): Boolean = {
    squares(0)(1) || squares(1)(0) || squares(1)(1) || squares(2)(1)
  }
  // take in a square of tiles and return the absolute direction of
  // the first true 4-neighbor, reverses list to prioritize clockwise selections
  def nearestNeighbor(squares: List[List[Boolean]]): Face.Value =
    (squares(0)(1), squares(1)(0), squares(1)(1), squares(2)(1)) match {
      case (_,_,_,true) => Down
      case (_,_,true,_) => Right
      case (_,true,_,_) => Left
      case (true,_,_,_) => Up
      case _ => Tsys.exitError("Error: nearestNeighbor nonexistant. Check isFourAdjacent() first");Up
    }

  def nextGridTileTrue(squares:List[List[Boolean]]): Boolean = face match {
    case Up => squares(0)(1)
    case Left => squares(1)(0)
    case Right => squares(1)(1)
    case Down => squares(2)(1)
  }

}

object GridEntity {
  import my.scalafx.EntityType._
  import my.scalafx.Face._

  def fromXML(gridEntityData: xml.Node): List[GridEntity] = {
    // convert our data to a list of entities
    gridEntityData
      .map(e => {
        // properties common to all entities
        val etypeString = (e \ "etype").text
        val xCoord = (e \ "xCoord").text.toInt
        val yCoord = (e \ "yCoord").text.toInt
        val faceString = (e \ "face").text
        val face: Face.Value = faceString match {
          case "Up"    => Up
          case "Down"  => Down
          case "Right" => Right
          case "Left"  => Left
        }
        val lastCoordsString = (e \ "lastCoord").text
        val lastCoordsList: List[Int] = lastCoordsString
          .filterNot(c => c == ')' || c == '(')
          .split(',')
          .map(_.toInt)
          .toList
        var lastCoord: (Int, Int) = (0, 0)
        if (lastCoordsList.length != 2)
          Tsys.exitError("Error: Read in an incomplete set of coords?")
        lastCoord = (lastCoordsList.head, lastCoordsList.last)
        // identify agent type
        val etype: EntityType.Value = etypeString match {
          case "agent"  => AgentEntity
          case "bridger" => BridgeEntity
        }
        val ret = etype match {
          case AgentEntity => {
            val agent = new Agent(xCoord, yCoord, face)
            val lastOnFringe = (e \ "lastOnFringe").text.toBoolean
            agent.setLastOnFringe(lastOnFringe)
            agent
          }
          case BridgeEntity => {
            val ourBridger = new Bridger(xCoord, yCoord, face)
            val lastOnFringe = (e \ "lastOnFringe").text.toBoolean
            ourBridger.setLastOnFringe(lastOnFringe)
            ourBridger
          }
        }
        ret.setLastCoord(lastCoord)
        ret
      })
      .toList
  }
}

object EntityType extends Enumeration {
  val AgentEntity, CircleEntity, BridgeEntity = Value
}

object Face extends Enumeration {
  val Up, Down, Left, Right = Value
  def rotate90(cf:Face.Value):Face.Value = {
    cf match {
      case Up => Right
      case Right => Down
      case Down => Left
      case Left => Up
    }
  }
}

// these correspond to actions such as move(face) and turn(Left)
object Action extends Enumeration {
  val Move, LeftTurn, RightTurn = Value
}
