package my.scalafx

import scalafx.application.JFXApp
import scalafx.application.JFXApp.PrimaryStage
import scalafx.geometry.Insets
import scalafx.scene.Scene
import scalafx.scene.effect.DropShadow
import scalafx.scene.layout.HBox
import scalafx.scene.paint.Color._
import scalafx.scene.paint._
import scalafx.scene.text.Text
import scalafx.scene.canvas._

import my.scalafx.Face._

// grid is 0-indexed (origin is 0,0 at the top-left)
class GridRenderer(gc: GraphicsContext) {
  // redraw the grid lines and reset the background
  def refreshGrid(csize: Int): Unit = {
    gc.fill = Color.White
    gc.fillRect(0, 0, csize, csize)
    gc.fill = Color.Black
    for (i <- 0 to csize if i % 21 == 0) {
      gc.fillRect(i, 0, 1, csize)
      gc.fillRect(0, i, csize, 1)
    }
  }

  // Render an oval in the grid at x,y
  def drawPelletAt(x: Int, y: Int): Unit = {
    gc.fill = Color.Green
    val xOffset = 9 + (x * 21)
    val yOffset = 9 + (y * 21)
    gc.fillRect(xOffset, yOffset, 3, 3)
  }

  // Render an oval in the grid at x,y
  def drawOvalAt(x: Int, y: Int): Unit = {
    gc.fill = Color.Black
    val xOffset = 2 + (x * 21)
    val yOffset = 2 + (y * 21)
    gc.fillOval(xOffset, yOffset, 18, 18)
  }

  def drawDirtAt(x: Int, y: Int): Unit = {
    gc.fill = Color.Brown
    val xOffset = 1 + (x * 21)
    val yOffset = 1 + (y * 21)
    gc.fillRect(xOffset, yOffset, 20, 20)
  }

  def drawMarkingAt(x: Int, y: Int): Unit = {
    gc.fill = Color.DarkKhaki
    val xOffset = 1 + (x * 21)
    val yOffset = 1 + (y * 21)
    gc.fillRect(xOffset, yOffset, 20, 20)
  }

  def drawFringeAt(x: Int, y: Int): Unit = {
    gc.fill = Color.Red
    val xOffset = 1 + (x * 21)
    val yOffset = 1 + (y * 21)
    gc.fillRect(xOffset, yOffset, 20, 20)
  }

  def drawTriAt(x: Int, y: Int, face: Face.Value, agent:Boolean): Unit = {
    if (agent) gc.fill = Color.Black else gc.fill = Color.Blue
    val dx = (11 + (x * 21)).toDouble
    val dy = (11 + (y * 21)).toDouble

    // triangle scaling values
    // front side of equilateral triangle has the lone point
    val frontSz = 7
    val backSz = 9

    val xPoints = face match {
      case Up    => Array(dx + backSz, dx - backSz, dx) // up
      case Down  => Array(dx + backSz, dx - backSz, dx) // down
      case Left  => Array(dx - backSz, dx + frontSz, dx + frontSz) // left
      case Right => Array(dx - frontSz, dx - frontSz, dx + backSz) // right
    }
    val yPoints = face match {
      case Up    => Array(dy + frontSz, dy + frontSz, dy - backSz) // up
      case Down  => Array(dy - frontSz, dy - frontSz, dy + backSz) // down
      case Left  => Array(dy, dy + frontSz, dy - backSz) // left
      case Right => Array(dy + backSz, dy - backSz, dy) // right

    }
    gc.fillPolygon(xPoints, yPoints, 3)
  }
}

object GridRenderer {}
