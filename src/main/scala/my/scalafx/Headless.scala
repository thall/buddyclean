package my.scalafx

import scala.xml._
import scala.io.Source

import my.scalafx.Grid._
import my.scalafx.Simulation._

class Headless(data: (String, String)) {
  def main(): Unit = {
    var isTestFile = false
    var demoMode = false
    val sims = if (data._1 == "load") {
      val simFile = data._2
      if (!Tsys.fileCheck(simFile))
        Tsys.exitError("File " + simFile + " not found")
      println("Loading: " + simFile)
      val xmlData = XML.loadFile(simFile)
      // load simulation
      val getSz = 40
      val getSquareSz = 22
      val canvasSz = 2 + (getSz * getSquareSz)
      // TODO: the get above values out of xmlData and resetup simulation
      val sim = Simulation.fromXML(None, xmlData)
      sim.runningFromFile
      List(sim)
    } else if (data._1 == "tests") {
      val testFile = data._2
      // does testFile exist?
      if (!Tsys.fileCheck(testFile))
        Tsys.exitError("File " + testFile + " not found")
      // get the list of simulation files from the testFile
      val simFiles = Source.fromFile(testFile).getLines.toList.map(_.trim)
      // test if simFiles actually exist
      simFiles.foreach(f =>
        if (!Tsys.fileCheck(f))
          Tsys.exitError(
            "File \"" + f + "\" not found when loading from " + testFile
          )
      )
      val ret = simFiles
        .map(fString => {
          val ret = Simulation.fromXML(None, XML.loadFile(fString))
          ret.runningFromFile
          ret
        })
        .toList
      isTestFile = true
      println("loaded " + ret.length + " simulations")
      ret
    } else { // run demos
      val grid = new Grid(40, 22)
      val sim = new Simulation(None, grid)
      sim.setup()
      demoMode = true
      List(sim)
    }
    sims.foreach(s => {
      val stats = runSim(s)
      println(" Complete!")
      println("")
      if (!demoMode) println(stats)
    })
    println("Done")
  }
  def runSim(sim: Simulation): String = {

    /** Running the Simulation * */
    var lastMsg = "Simulating..."
    print(lastMsg)
    while (!sim.isFinished) {
      sim.update()
      sim.logTickData()
      if (lastMsg != sim.getMsg()) {
        lastMsg = sim.getMsg
        if (lastMsg != "None") println(lastMsg)
      }
    }
    val endTick = sim.getTick
    var ret = "-Stats- \n"
    val resultTime =
      if (endTick < sim.getTickLimit) "Finished at tick: " + endTick + "\n"
      else "Stopped at tick limit of " + sim.getTickLimit + "\n"
    val (onDirtAvg, offDirtAvg, onFringeAvg, idleAvg, trappedAvg) = sim.averages
    val resDirt = "> on dirt: " + onDirtAvg + "\n"
    val resOffDirt = "> not on dirt: " + offDirtAvg + "\n"
    val resFringe = "> on fringe: " + onFringeAvg + "\n"
    val resIdle = "> idle: " + idleAvg + "\n"
    val resTrapped = "> trapped: " + trappedAvg + "\n"
    val cleaned =
      if (sim.getGrid.clean()) "Grid Cleaned!" + "\n"
      else "Grid still dirty. \n"

    "-Stats- \n" + resultTime + cleaned + "--Average agents __ per tick-- \n" + resFringe + resIdle + resTrapped +
      resOffDirt + resDirt
  }
}
