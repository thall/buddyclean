package my.scalafx

object Main {
  def main(args: Array[String]): Unit = {
    if (args.length == 0) printUsage
    // string tuples, flag and argument each
    val cmds: List[(String, String)] = runOpts(args)
    // check graphicsMode and demoMode flags
    var graphicsMode = false
    var demoMode = false
    val (nArgs, yArgs) = cmds.partition(_._2 == "")
    nArgs.foreach(x =>
      x._1.toList match {
        case List('-', 'g') => graphicsMode = true
        case List('-', 'd') => demoMode = true
        case List('-', 'h') => printUsage // if there is any h flag, print usage
        case x :: y :: zs => {
          val flagLetters = y :: zs
          if (flagLetters.contains('h')) printUsage
          if (flagLetters.contains('d')) demoMode = true
          if (flagLetters.contains('g')) graphicsMode = true
        }
        case _ => printUsage
      }
    )
    // run demo modes
    if (demoMode && graphicsMode) new Graphics(("", "")).main(Array())
    else if (demoMode) new Headless(("", "")).main()
    // run from files
    val isLoading = yArgs.map(_._1).contains("load")
    val isTesting = yArgs.map(_._1).contains("tests")
    if (isLoading) {
      // load simulation from file
      // take the argument of the -f flag
      val simfile = yArgs.filter(_._1 == "load").head._2.drop(9).dropRight(1)
      if (!Tsys.fileCheck(simfile))
        Tsys.exitError("Error: File not found at: " + simfile)
      val fileArgs = ("load", simfile)
      if (graphicsMode) new Graphics(fileArgs).main(Array())
      else new Headless(fileArgs).main()
    } else if (isTesting) {
      // load file listing simulation files to load as tests
      val testsListingfile =
        yArgs.filter(_._1 == "tests").head._2.drop(9).dropRight(1)
      val fileArgs = ("tests", testsListingfile)
      new Headless(fileArgs).main()
    } else if (!demoMode && !isLoading && !isTesting) {
      printUsage
    }
  }

  // return a list of actions to take
  private def runOpts(args: Array[String]): List[(String, String)] = {
    // this works because no flag has multiple arguments
    val (flgs, argz) = args.toSeq.partition(a => a.head == '-')
    val argbuffer = scala.collection.mutable.ArrayBuffer(argz).map(_.toString)
    val ret: List[(String, String)] = flgs
      .map(flag =>
        flag match {
          case "-f" => {
            val arg = argbuffer.head
            argbuffer.remove(0)
            ("load", arg)
          }
          case "-t" => {
            val arg = argbuffer.head
            argbuffer.remove(0)
            ("tests", arg)
          }
          case x => (x, "")
        }
      )
      .toList
    ret
  }

  private def printUsage(): Unit = {
    val tabIndent = "     "
    println("Usage: java -jar buddyclean.jar [OPTION]")
    println("Run the buddyclean simulation of the DCC problem. \n")
    println("""-f [SIMFILE.xml] """ + tabIndent + "loads simulation file")
    println(
      """-t [TESTLIST]    """ + tabIndent + "loads simulation files specified in test list file"
    )
    println("""-g               """ + tabIndent + "run with GUI")
    println("""-d               """ + tabIndent + "run in graphical demo mode")
    println("""-h               """ + tabIndent + "show this help dialog")
    println(
      """Example Usage:   """ + tabIndent + "java -jar buddyclean.jar -gd"
    )
    System.exit(0)
  }
}
