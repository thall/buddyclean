package my.scalafx

import scala.collection.mutable.ListBuffer

import my.scalafx.Grid._
import my.scalafx.GridRenderer._
import my.scalafx.GridEntity._
import my.scalafx.EntityType._
import my.scalafx.Face._

class Simulation(gr: Option[GridRenderer], grid: Grid) {

  val tickLimitDefault = 1600
  val spreadTicksDefault = 0
  private var gridAgents = collection.mutable.Buffer[GridEntity]()
  private var testCycle = 0
  private var tick = 0
  private var tickLimit = tickLimitDefault
  private var lastMsg = "None"
  private var findingFringeMode = false

  /**    spreadTicks is the number of ticks before each dirt spread event.
    *    setting spreadTicks to 0 (default) disables dirt spread.
    *    spreadTickCounter is the number of ticks until the next spread event,
    *    which is reset to spreadTicks after each spread event.
    *    spread occurs when spreadTickCounter == 0
    */
  private var spreadTickCounter = 1
  private var spreadTicks = spreadTicksDefault
  def getSpreadTicks():Int = spreadTicks
  val defaultLookAhead = 3
  val maxLookAhead = 10
  private var finished = false
  private var fromFile = false

  /**    Values used for logging and creating averages during testing.
    *    Lists log the number of agents which fit that category per tick
    *    (integer number per index).
    *    In some cases no log is taken unless it is a spread tick
    *    (newly trapped agents, etc).
    */
  private var agentsOnDirtLog: ListBuffer[Int] = ListBuffer()
  def getAgentsOnDirtLog():ListBuffer[Int] = agentsOnDirtLog
  private var agentsOffDirtLog: ListBuffer[Int] = ListBuffer()
  def getAgentsOffDirtLog():ListBuffer[Int] = agentsOffDirtLog
  private var agentsOnFringeLog: ListBuffer[Int] = ListBuffer()
  def getAgentsOnFringeLog():ListBuffer[Int] = agentsOnFringeLog
  private var idleLog: ListBuffer[Int] = ListBuffer()
  def getIdleLog():ListBuffer[Int] = idleLog
  private var trappedLog: ListBuffer[Int] = ListBuffer()
  def getTrappedLog():ListBuffer[Int] = trappedLog

  def update() {
    // send agents grid info
    sendGridInfo()
    // draw
    if (gr != None) {
      grid.drawDirt(gr.get)
      grid.drawMarkings(gr.get)
      drawEntities(gr.get)
    }
    tick += 1
    // dirt spread is enabled
    if (spreadTicks != 0) {
      spreadTickCounter -= 1
      // if counter is at zero spread dirt and reset counter
      if (spreadTickCounter == 0) {
        grid.spread()
        spreadTickCounter = spreadTicks
      }
      // clean grid of pheromone markings every 40 ticks if spread disabled.
      // for testing
    } else if (tick%40==0) grid.clearMarkings
    // cycle tests if grid is clean or simulation timed out
    if (
      !fromFile && !findingFringeMode && (tick >= tickLimit || grid
        .clean() || ooBounds())
    ) {
      tickLimit = tickLimitDefault
      tick = 0
      spreadTicks = spreadTicksDefault
      cycleTestSetup()
    }
    if (
      fromFile && !findingFringeMode && ((tick >= tickLimit || grid
        .clean() || ooBounds()))
    ) {
      finished = true
    }
  }

  /**    1. Poll every agent for their predicted destination, and filter by
    *    priority to determine which agents get to act this tick.
    *    2. For each grid agent send them info about their surrounding tiles
    *    which they will use to send back the tiles which were cleaned
    *    from the grid and as a side effect, act.
    *    3. Update which tiles are occupied for rendering.
    */
  private def sendGridInfo(): Unit = {
    // group agents by destination, and only allow the agent with the highest priority per destination
    // to act
    val actingAgents = gridAgents
      .groupBy(a =>
        a.nextCoord(
          findFrontCoords(a),
          gridInfo(a)._1,
          grid.at(a.x, a.y),
          grid.at(a.last)
        )
      )
      .map(a => a._2.sortWith(_.priority > _.priority))
      .map(a => a.head)
    // agents now act
    actingAgents.foreach(e => {
      val delX = e.x
      val delY = e.y
      // movement behaviors are in the entity's process function
      val toClean = e.process(
        findFrontCoords(e),
        gridInfo(e),
        (grid.at(e.x, e.y),grid.markingAt(e.x,e.y)),
        (grid.at(e.last),grid.markingAt(e.last)),
        grid.forwardScan(e, bridgersLookAhead)
      )
      if (e.toPlaceMarking) grid.addMarking(e.x,e.y)
      // clean if we aren't a fringefinding simulation
      if (toClean && !findingFringeMode) grid.removeDirt(delX, delY)
    })
    // update occupied tiles
    gridAgents.foreach(a => a.occupy(a.x, a.y))
  }

  private def bridgersLookAhead():Int = {
    if (spreadTicks != 0 && spreadTicks <= maxLookAhead) spreadTicks+1
    else if (spreadTicks != 0) maxLookAhead
    else defaultLookAhead
  }

  // update per-tick data
  def logTickData(): Unit = {
    val agentsOnFringe = gridAgents.filter(a => grid.isOnFringe((a.x, a.y)))
    agentsOnFringeLog += agentsOnFringe.length
    val (agentsOnDirt, agentsOffDirt) =
      gridAgents.map(a => grid.at((a.x, a.y))).partition(_ == true)
    agentsOnDirtLog += agentsOnDirt.length
    agentsOffDirtLog += agentsOffDirt.length
    val idleAgents = gridAgents.filter(a => a.location == a.last)
    idleLog += idleAgents.length
    val isSpreadTick = if (spreadTickCounter == 0) true else false
    if (isSpreadTick) {
      // They are newly trapped if they were on the fringe, but now they are not, and they are on a dirt tile
      val newlyTrappedAgents = gridAgents.filter(a =>
        a.lastOnFringe && grid.at((a.x, a.y)) && !grid.isOnFringe((a.x, a.y))
      )
      trappedLog += newlyTrappedAgents.length
    }
  }

  def averages(): (Double, Double, Double, Double, Double) = {
    (
      giveAvg(agentsOnDirtLog),
      giveAvg(agentsOffDirtLog),
      giveAvg(agentsOnFringeLog),
      giveAvg(idleLog),
      giveAvg(trappedLog)
    )
  }

  def isFinished(): Boolean = finished

  def runningFromFile(): Unit = fromFile = true

  def cycleTestSetup() {
    // reset Sim to neutral, empty state
    gridAgents.clear()
    grid.clearDirt()
    testCycle += 1
    lastMsg = "Cycling Demos, running test: " + testCycle
    testCycle match {
      case 1 => fuzzySpreadSetupBridgers()
      case 2 => fuzzySpreadSetup()
      case 3 => criticalSetup()
      case 4 => saNoSpreadSetup()
      case 5 => saSpreadSetup()
      case 6 => maNoSpreadSetup()
      case 7 => maSpreadSetup()
      case 8 => sBridgerNoSpreadSetup()
      case 9 => mBridgerNoSpreadSetup() 
      case _ => {
        lastMsg = "Demos Completed"
        finished = true
      }
    }
  }

  def getGrid(): Grid = grid

  def getMsg(): String = lastMsg

  def getTick(): Int = tick

  def setTick(t:Int): Unit = tick = t

  def getTickLimit(): Int = tickLimit

  def setTickLimit(tL:Int): Unit = tickLimit = tL

  // setup different scenarios for testing
  def setup() {
    // setup an initial scenario here
    // setup to test critical tile detection
    spreadTicks = 0
    tickLimit = 2; // nothing here yet so just a small tick limit
    sBridgerNoSpreadSetup()
  }

  // find the fringe of the grid
  def scoutFringe() {
    findingFringeMode = true
    spreadTicks = 0
    // add an agent
    val (ax, ay) = grid.fetchAFringeCoord()
    val agentDirection = Right // this may be a bad guess
    addEntity(AgentEntity, ax, ay, Left)
  }

  def fetchFringeList(): Option[List[((Int, Int), Face.Value)]] = {
    val fList = gridAgents.head.fetchMoveLog
    if (fList.isEmpty) None else Some(fList)
  }

  // setup to test critical tile detection
  def criticalSetup() {
    tickLimit = 15
    spreadTicks = 0
    grid.dirtRect((4, 4), (9, 4))
    addEntity(AgentEntity, 5, 4, Right)
  }

  // multiple agents with spread disabled setup
  def maNoSpreadSetup() {
    tickLimit = 125
    spreadTicks = 0
    grid.dirtRect((11, 11), (31, 31))
    grid.dirtRect((13, 8), (14, 31))
    autoPlaceAgents(25)
  }

  // setup with a single agent around a shape, spread disabled
  def saNoSpreadSetup() {
    tickLimit = 200
    spreadTicks = 0
    grid.dirtRect((1, 1), (10, 10))
    grid.dirtRect((5,10),(8,20))
    grid.dirtRect((5,15),(13,19))
    addEntity(AgentEntity, 1, 1, Right)
  }

  // setup with a single bridger around a shape, spread disabled
  def sBridgerNoSpreadSetup() {
    tickLimit = 600
    spreadTicks = 0
    grid.dirtRect((1, 1), (10, 10))
    grid.dirtRect((5,10),(8,20))
    grid.dirtRect((5,15),(13,19))
    grid.dirtRect((9,10),(11,12))
    addEntity(BridgeEntity, 1, 1, Right)
    addEntity(AgentEntity, 1, 6, Up)
  }

  // setup with a single bridger around a shape, spread enabled
  def mBridgerNoSpreadSetup() {
    setSpreadTick(40)
    tickLimit = 600
    grid.dirtRect((1, 1), (10, 10))
    grid.dirtRect((5,10),(8,20))
    grid.dirtRect((5,15),(13,19))
    grid.dirtRect((9,10),(11,12))
    addEntity(BridgeEntity, 1, 1, Right)
  }

  // setup with a single agent around a shape, spread enabled
  def saSpreadSetup() {
    setSpreadTick(40) // enable dirt spread and set to 20 ticks
    tickLimit = 600
    grid.dirtRect((15, 15), (25, 25))
    grid.dirtRect((24,24),(28,28))
    grid.dirtRect((12,12),(16,16))
    addEntity(AgentEntity, 15, 15, Right)
    addEntity(AgentEntity, 25, 25, Left)
  }

  // setup with multiple agents around a spreading shape
  def maSpreadSetup() {
    tickLimit = 280
    setSpreadTick(20) // enable dirt spread and set to x ticks
    grid.dirtRect((13, 13), (24, 24))
    grid.dirtRect((24, 14), (30, 17))
    grid.dirtRect((24, 21), (30, 24))
    autoPlaceAgents(18)
  }

  def fuzzySpreadSetup() {
    tickLimit = 500
    setSpreadTick(12)
    grid.dirtRect((15,15),(30,30))
    grid.dirtRect((13,16),(15,18))
    grid.dirtRect((13,20),(15,22))
    grid.dirtRect((13,24),(15,26))
    grid.dirtRect((13,28),(15,29))
    autoPlaceAgents(18)
  }

  def fuzzySpreadSetupBridgers() {
    tickLimit = 500
    setSpreadTick(12)
    grid.dirtRect((15,15),(30,30))
    grid.dirtRect((13,16),(15,18))
    grid.dirtRect((13,20),(15,22))
    grid.dirtRect((13,24),(15,26))
    grid.dirtRect((13,28),(15,29))
    addEntity(AgentEntity,18,15,Right)
    addEntity(AgentEntity,17,30,Left)
    addEntity(BridgeEntity,14,24,Up)
    addEntity(AgentEntity,15,21,Down)
    addEntity(AgentEntity,15,21,Down)
  }

  def addEntity(
      eType: EntityType.Value,
      x: Int,
      y: Int,
      face: Face.Value
  ): Unit = {
    val newGridEntity: GridEntity =
      eType match {
        case AgentEntity  => new Agent(x, y, face)
        case BridgeEntity => new Bridger(x, y, face)
      }
    gridAgents.addOne(newGridEntity)
  }

  def importEntity(e: GridEntity) = gridAgents.addOne(e)

  private def giveAvg(ls: ListBuffer[Int]): Double = {
    if (!ls.isEmpty) {
      val count = ls.length
      val sum = ls.foldLeft(0)(_ + _)
      sum / count
    } else -1
  }

  // if any agents are out of bounds, return true
  private def ooBounds(): Boolean = {
    gridAgents.exists(a => (a.x < 0) || (a.y < 0))
  }

  def setSpreadTick(t: Int): Unit = {
    spreadTicks = t
    spreadTickCounter = t
  }

  def setSpreadTickCounter(t: Int): Unit = spreadTickCounter = t

  /** Return two grids of all the coordinates around the entity except current tile
    the first grid in the tuple is the dirt grid, the second is the marker grid
    all 3x3 grid squares except the center are represented, from top to bottom
    ex: List(List(false, true, true), List(false, true), List(false, true, true))
  */
  private def gridInfo(e: GridEntity): (List[List[Boolean]],List[List[Boolean]]) = {
    val coords = List(
      List((e.x - 1, e.y - 1), (e.x, e.y - 1), (e.x + 1, e.y - 1)),
      List((e.x - 1, e.y), (e.x + 1, e.y)),
      List((e.x - 1, e.y + 1), (e.x, e.y + 1), (e.x + 1, e.y + 1))
    )
    (coords.map(x => x.map(grid.at)), coords.map(x => x.map(grid.markingAt)))
  }

  /**    Returns a list of the dirt values of three coordinates in front of entity
    *    as well as the last coordinates being the dirt values to its left then right
    *    for example: if to the front of an entity, from left to right there was
    *    Empty, Dirt, Dirt
    *    Empty,Agent, Dirt
    *    we'd return
    *    List(False, True, True, False, True)
    */
  private def findFrontCoords(e: GridEntity): List[Boolean] = {
    val (x, y) = e.advanceCoord()
    val fronts: List[(Int, Int)] =
      e.face match {
        case Up =>
          List((x - 1, y), (x, y), (x + 1, y), (x - 1, y + 1), (x + 1, y + 1))
        case Down =>
          List((x + 1, y), (x, y), (x - 1, y), (x + 1, y - 1), (x - 1, y - 1))
        case Left =>
          List((x, y + 1), (x, y), (x, y - 1), (x + 1, y + 1), (x + 1, y - 1))
        case Right =>
          List((x, y - 1), (x, y), (x, y + 1), (x - 1, y - 1), (x - 1, y + 1))
      }
    fronts.map(grid.at)
  }

  private def drawEntities(gr: GridRenderer): Unit = {
    gridAgents.foreach(e =>
      e.entityType match {
        case AgentEntity  => gr.drawTriAt(e.x, e.y, e.face, true)
        case CircleEntity => gr.drawOvalAt(e.x, e.y)
        case BridgeEntity => {
          // agent
          gr.drawTriAt(e.x,e.y,e.face,false)
          // draw pellets in front of agent to see its "lookAhead"
          val peekPelletCoords = grid.forwardScan(e, bridgersLookAhead)
          peekPelletCoords.foreach(c => gr.drawPelletAt(c._2._1,c._2._2))
        }
      }
    )
  }

  // place an agent every gapSpaces along the fringe
  private def autoPlaceAgents(gapSpaces: Int): Unit = {
    val fringe: List[((Int, Int), Face.Value)] = grid.fringeList
    // filter out every gapSpaces element of the fringe
    // I may be doing the opposite of gaps
    val indexed = fringe.zipWithIndex
    val filtered = indexed.filter(x => ((x._2 % gapSpaces) == 0))
    val (placements, _) = filtered.unzip
    placements.foreach(x => {
      this.addEntity(AgentEntity, x._1._1, x._1._2, x._2)
    })
  }

  def toXML(): xml.Node = {
    <sim>
    <data>
      {logToXML(agentsOnDirtLog,"ondirt").get}
      {logToXML(agentsOffDirtLog,"offdirt").get}
      {logToXML(agentsOnFringeLog,"fringe").get}
      {logToXML(idleLog,"idle").get}
    {
      val trapped = logToXML(trappedLog,"trapped")
      if (trapped!=None) trapped.get
    }
    </data>
      <entities>
      {gridAgents.map(_.toXML())}
      </entities>
      {grid.toXML()}
      <tick>{tick}</tick>
      <tickLimit>{tickLimit}</tickLimit>
      <lastMsg>Loaded from XML!</lastMsg>
      <spreadTickCounter>{spreadTickCounter}</spreadTickCounter>
      <spreadTicks>{spreadTicks}</spreadTicks>
    </sim>
  }

  private def logToXML(log:ListBuffer[Int],label:String): Option[xml.Elem] = {
    if (!log.isEmpty) {
      Some(
      <log label={label}>
        {log.map(x => {<p>{x}</p>})}
      </log>)
    } else {None}
  }

  def setLogData(data:xml.NodeSeq):Unit = {
    // for each log, add the data back into the simulation
    (data \ "log").foreach(log => {
      val data = (log \ "p")
      (log \ "@label").text match {
        case "ondirt" => data.foreach(p => agentsOnDirtLog += p.text.toInt)
        case "offdirt" => data.foreach(p => agentsOffDirtLog += p.text.toInt)
        case "fringe" => data.foreach(p => agentsOnFringeLog += p.text.toInt)
        case "idle" => data.foreach(p => idleLog += p.text.toInt)
        case "trapped" => data.foreach(p => trappedLog += p.text.toInt)
        case _ => Tsys.exitError("Error: Invalid logdata label "+(log \ "@label").text)
      }
    })
  }

  override def equals(that:Any):Boolean = that match {
    case that: Simulation => {
      val allMustBeTrue = List(
        this.tick == that.getTick,
        this.spreadTicks == that.getSpreadTicks,
        this.agentsOnDirtLog == that.getAgentsOnDirtLog,
        this.agentsOnFringeLog == that.getAgentsOnFringeLog,
        this.idleLog == that.getIdleLog,
        this.trappedLog == that.getTrappedLog
      )
      if (allMustBeTrue.contains(false)) false else true
    }
    case _ => false
  }

}

object Simulation {

  def fromXML(
      gridRenderer: Option[GridRenderer],
      file: xml.Elem
  ): Simulation = {
    val ourGrid: Grid = Grid.fromXML((file \ "grid"))
    var ourSim = new Simulation(gridRenderer, ourGrid)
    ourSim.setLogData((file \ "data"))
    ourSim.setTickLimit((file \ "tickLimit").text.toInt)
    ourSim.setSpreadTick((file \ "spreadTicks").text.toInt)
    ourSim.setSpreadTickCounter((file \ "spreadTickCounter").text.toInt)
    ourSim.setTick((file \ "tick").text.toInt)
    val gridEntities =
      (file \ "entities" \ "gridEntity").map(e => GridEntity.fromXML(e))
    gridEntities.flatten.foreach(e => ourSim.importEntity(e))
    ourSim
  }
}
