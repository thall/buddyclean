package my.scalafx

import java.io.File

/**  Various system functions I use.
  */

object Tsys {

  // Exit the program with description of an error
  def exitError(error: String): Unit = {
    Console.err.println(Console.RED + error + Console.RESET)
    sys.exit(1)
  }

  // check if a file exists
  def fileCheck(path: String): Boolean = {
    val fileInQuestion = new File(path)
    fileInQuestion.exists
  }

}
