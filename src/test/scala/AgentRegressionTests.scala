package my.scalafx

import org.scalatest.funsuite.AnyFunSuite
import scala.io.Source
import scala.xml._

import my.scalafx.Simulation

class AgentRegressionTests extends AnyFunSuite {
  val testFiles = Source.fromFile("testSims/testsListing").getLines.toList.map(_.trim)
  test("tests found") {
    val filesFound = testFiles.map(Tsys.fileCheck(_))
    assert(!filesFound.contains(false))
  }
  // create and run simulations from test simulation files
  val sims = testFiles.map(file => {
    val ret = Simulation.fromXML(None, XML.loadFile(file))
      ret.runningFromFile
      ret
  })
  sims.foreach(sim => {
    while(!sim.isFinished) {
      sim.update()
      sim.logTickData()
    }
  })
  // optionally export a new test case here for regression comparisons later
  // sims.zipWithIndex.foreach(x => XML.save(x._2.toString+"- test", x._1.toXML(),"UTF-8"))

  // Now we test these against the expected results
  val finishedSims = sims.zip(testFiles)

  // get the expected results
  val resultFiles = Source.fromFile("testSims/expectedResults/resultsListing").getLines.toList.map(_.trim)
  val resultSims = resultFiles.map(file =>{
    val ret = Simulation.fromXML(None, XML.loadFile(file))
    ret.runningFromFile
    ret
  })
  // not checking for order, assuming tests were loaded in the same order/amount
  val expectedSims = resultSims.zip(testFiles)
  val simPairs = finishedSims.zip(expectedSims)
  simPairs.foreach(x => {
    val simName = x._1._2
    val sim = x._1._1
    val expectedSim = x._2._1
    test("Simulation: "+simName+" matches expected results") {
      // (re-enable after making new test cases) assert(sim.equals(expectedSim))
    }
  })
}
